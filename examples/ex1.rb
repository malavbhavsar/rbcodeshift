require 'pry'
require_relative '../lib/codeshift'

class Ex1
  extend Codeshift::NodePattern::Macros

  def_node_matcher :expect_subject_something_match?,<<-PATTERN
    (send nil? :expect (send (send nil? :subject) _))
  PATTERN

  def_node_search :capture_subject_search,<<-PATTERN
    (send nil? :expect (send $(send nil? :subject) _))
  PATTERN

  def_node_search :find_old_style_it_blocks, <<-PATTERN
    (block
      (send nil? :it)
      args
      (send #expect_subject_something_match? ...)
    )
  PATTERN

  def_node_search :capture_blk_expr_search,<<-PATTERN
    (block
      (send nil? :it)
      args
      $(send #expect_subject_something_match? ...)
    )
  PATTERN

  def workwork
    filepath = "#{__dir__}/fixtures/before/audit_self_serve_controller_spec.rb"

    buffer = Parser::Source::Buffer.new(filepath)
    buffer.source = File.read(filepath)
    parser = Parser::Ruby24.new(Codeshift::AST::Builder.new)
    root_node = parser.parse(buffer)
    old_style_it_nodes = find_old_style_it_blocks root_node
    corrector = Codeshift::Corrector.new(buffer, [])
    old_style_it_nodes.each do |node|
      do_process(node, corrector)
    end
    new_source = corrector.rewrite
    new_filename = "#{__dir__}/fixtures/after/audit_self_serve_controller_spec.rb"
    File.open(new_filename, 'w') { |f| f.write(new_source) }
  end

  def do_process(node, corrector)
    matched_subject_nodes = capture_subject_search(node)
    matched_subject_nodes.each do |sn|
      corrector.replace(sn.source_range, "response")
    end

    parent_indent = node.loc.column.times.collect { ' ' }.join
    one_indent = '  '
    b = node.loc.begin
    e = node.loc.end

    # fix the whitespace hacks
    corrector.insert_before(b, ' ') unless whitespace_before?(b)
    if whitespace_after?(b, 1)
      corrector.replace(b, "do\n#{parent_indent}#{one_indent}subject\n#{parent_indent} ")
    else
      corrector.replace(b, "do\n#{parent_indent}#{one_indent}subject\n#{parent_indent}  ")
    end

    if whitespace_before?(e)
      corrector.replace(e.adjust(begin_pos: -1), "\n#{parent_indent}end")
    else
      corrector.replace(e, "\n#{parent_indent}end")
    end
  end

  def whitespace_before?(range)
    range.source_buffer.source[range.begin_pos - 1, 1] =~ /\s/
  end

  def whitespace_after?(range, length = 1)
    range.source_buffer.source[range.begin_pos + length, 1] =~ /\s/
  end

  def remove_inblk_leading_whitespace(blknode, corrector)
    corrector.remove_preceding(
      blknode.body.source_range,
      blknode.body.source_range.begin_pos -
        blknode.send_node.source_range.end_pos
    )
  end

  def remove_inblk_trailing_whitespace(blknode, corrector)
    corrector.remove_preceding(
      blknode.loc.end,
      blknode.loc.end.begin_pos - blknode.body.source_range.end_pos
    )
  end

  private
  # util method to traverse nodes in AST
  def noden(node, *descendentnumbers)
    descendentnumbers.each do |num|
      node = node.children[num]
    end
    node
  end
end

Ex1.new.workwork
