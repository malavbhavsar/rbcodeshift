require 'integration_helper'

RSpec.describe Markets::AuditSelfServeController, type: :controller do
  subject { get :index }

  before do
    auth_doorman('user', ['support_user'])
  end

  describe 'auth' do
    subject { get(:index) }

    context 'without the proper auth' do
      context 'with no auth' do
        before { auth_doorman('user') }
        it { expect(subject.status).to eq(403) }
      end

      context 'with unrelated/bad auth' do
        before { auth_doorman('user', ['foobar']) }
        it { expect(subject.status).to eq(403) }
      end
    end

    context 'with the proper auth' do
      context 'with admin' do
        before { auth_doorman('user', ['admin']) }
        it { expect(subject.status).to eq(200) }
      end

      context 'with support_user' do
        before { auth_doorman('user', ['support_user']) }
        it { expect(subject.status).to eq(200) }
      end

      context 'with operations' do
        before { auth_doorman('user', ['operations']) }
        it { expect(subject.status).to eq(200) }
      end
    end
  end

  describe '#index' do
    let(:time) { Time.utc(2015, 1, 2) }
    let(:plan) { Timecop.freeze(time) { create :active_plan_with_advance } }
    let(:plan2) { Timecop.freeze(3.days.from_now(time))  { create :active_plan_with_advance } }
    let!(:plan_assessment_cefe) { Timecop.freeze(time) { create :assessment_cefe, plan_id: plan.id } }

    it 'should return the agreements for the chosen plans' do
      get :index, params: { agreements: '', plan_ids: "#{plan.id}, #{plan2.id}" }

      expect(response).to be_success
      expect(response.content_type).to eq "application/zip"
      Zip::InputStream.open(response.stream.to_path) do |io|
        file1 = io.get_next_entry
        expect(file1).to be_present
        expect(file1.name).to eq "plan_#{plan.id}.pdf"
        expect(io.read).to eq plan.contract.load_pdf
        file2 = io.get_next_entry
        expect(file2).to be_present
        expect(file2.name).to eq "plan_#{plan2.id}.pdf"
        expect(io.read).to eq plan2.contract.load_pdf
        file3 = io.get_next_entry
        expect(file3).to_not be_present
      end
    end

    it 'should return the CEFEs and Ledger entries for the given plans and time range' do
      get :index, params: { ledger: '', plan_ids: "#{plan.id}, #{plan2.id}", start_time: '2015-1-1', end_time: '2015-1-3' }

      expect(response).to be_success
      expect(response.content_type).to eq "application/zip"
      Zip::InputStream.open(response.stream.to_path) do |io|
        file1 = io.get_next_entry
        expect(file1).to be_present
        expect(file1.name).to eq "plan_#{plan.id}.csv"
        expect(io.read).to eq(
          "created at, event type, amount, ledger_created_at, ledger_type, ledger_to_user, ledger_to_fee\n"\
          "2015-01-02 00:00:00 UTC, ADVANCE, 100000,,,,\n"\
          "2015-01-02 00:00:00 UTC, REPAYMENT, -1000, 2015-01-02 00:00:00 UTC, CAPTURE, 9725, 275\n"
        )
        file2 = io.get_next_entry
        expect(file2).to be_present
        expect(file2.name).to eq "plan_#{plan2.id}.csv"
        expect(io.read).to eq(
          "created at, event type, amount, ledger_created_at, ledger_type, ledger_to_user, ledger_to_fee\n"
        )
        file3 = io.get_next_entry
        expect(file3).to_not be_present
      end
    end

    it { is_expected.to be_success }

    context 'when time input is wrong' do
      subject { get :index, params: { ledger: '', plan_ids: "#{plan.id}, #{plan2.id}", start_time: 'something', end_time: '' } }

      it 'should return bad request' do
        subject
        expect(response.code).to eq '400'
      end
    end

    context 'when the current user is not authorized' do
      before do
        auth_doorman('user')
      end

      it 'should return a 403 response code' do
        expect(subject.code).to eql('403')
      end
    end
  end
end
