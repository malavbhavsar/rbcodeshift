# frozen_string_literal: true

module Codeshift
  module AST
    # A node extension for `sym` nodes. This will be used in  place of a
    # plain node when the builder constructs the AST, making its methods
    # available to all `sym` nodes within Codeshift.
    class SymbolNode < Node
      include BasicLiteralNode
    end
  end
end
