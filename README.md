# rb-codeshift

Make sweeping changes easy. A lot of code is copied from rubocop.

# How to run this **DEMO**?
As a PoC, we are trying to change patterns like
```ruby
  it { expect(subject.status).to eq(403) } 
```
to
```ruby
  it do
    subject
    expect(response.status).to eq(403)
  end
```

1. `bundle install`

2. `ruby examples/ex1.rb`

3. Check the diff! `vim -d examples/fixtures/after/audit_self_serve_controller_spec.rb examples/fixtures/ideal_after/audit_self_serve_controller_spec.rb`
